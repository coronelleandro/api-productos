const express = require('express');
const router = express.Router();

const productsCtrl = require('../Controller/products.controller');


router.get('/',productsCtrl.getProducts);

router.get('/:id',productsCtrl.getOneProduct);

router.post('/create',productsCtrl.createProduct);

router.put('/update/:id',productsCtrl.updateProduct);

router.delete('/delete/:id',productsCtrl.deleteProduct);

module.exports = router;
const express = require('express');
const morgan = require('morgan');
const app = express();
const { mongoose } = require('./database');
//settings
app.set('port',3000);

//middlewares
app.use(morgan('dev'));
app.use(express.json());

//routes
app.use(require('./routes/products.routes'));

//starting the server
app.listen(app.get('port'),()=>{
    console.log('servidor iniciado en puerto: '+ app.get('port'));
});


const Product = require('../Models/products');

const productsCtrl = {};

productsCtrl.getProducts = async (req,res)=>{
    const products = await Product.find();
    res.json(products);
}

productsCtrl.createProduct = async (req,res)=>{
    //res.json("recibido");
    const pro = new Product(req.body);
    await pro.save();
    res.json({
        status : 'create'
    });
}

productsCtrl.getOneProduct = async (req,res)=>{
    const pro = await Product.findById(req.params.id);
    res.json(pro);
}

productsCtrl.updateProduct = async (req,res)=>{
    const {id} = req.params;
    const pro = {
        name: req.body.name,
        description : req.body.description,
        stock : req.body.stock,
        price : req.body.price
    };
    await Product.findByIdAndUpdate(id, {$set : pro}, {new : true});
    res.json({
        status : 'update'
    });
}

productsCtrl.deleteProduct = async (req,res)=>{
    await Product.findByIdAndRemove(req.params.id);
    res.json({
        status : 'remove'
    });
}

module.exports = productsCtrl;
const mongoose = require('mongoose');
const uri = 'mongodb://localhost/productos';

mongoose.connect(uri)
    .then(db => console.log('Base de datos esta conectada'))
    .catch( err => console.error(err));

module.exports = mongoose;